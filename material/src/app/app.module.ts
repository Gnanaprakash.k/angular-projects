import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { ToastComponent } from './toast/toast.component';  
import { prakash } from 'src/Material/material.module';



@NgModule({
  declarations: [
    AppComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    prakash,
    ToastrModule.forRoot()
    

   
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
