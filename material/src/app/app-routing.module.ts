import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { ToastComponent } from './toast/toast.component';

const routes: Routes = [{path:'toast',component:ToastComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
