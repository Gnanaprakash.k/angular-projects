import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';  

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css']
})
export class ToastComponent implements OnInit {
message:"work" | undefined;
title: "ok"|undefined;
showspinner=false;
 displayedColumns: string[] = ['EmployeeName', 'Employeeage', 'Gender', 'EmployeePostion'];

open=false;
  constructor(private toast:ToastrService) { }

  check:any=[
    {
      Name:'Prakash',
      age:'24',
      gender:'male',
      position:'dev',
    },
    {
      Name:'Prakash',
      age:'24',
      gender:'male',
      position:'dev',
    }

    
  ]

  ngOnInit(): void {
  }

  showToaster(){  
    this.toast.success("Hello, I'm the toastr message")   
}  
showSuccess(message: "work" | undefined, title: "ok" | undefined){  
  this.toast.success(message, title)  
}  

loadData(){
  this.showspinner=true; 
  setTimeout(()=>{this.showspinner=false},10000); 
}
 sidenavasaveconsole(state: any){
  console.log(state);
  
 } 

 userForm = new FormGroup({

  firstName: new FormControl(''),

  lastName: new FormControl(''),

  age: new FormControl(''),

  email: new FormControl(''),



});

onSubmit(){



console.log(this.userForm.value);

}



}
