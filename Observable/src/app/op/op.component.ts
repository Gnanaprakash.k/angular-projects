import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { filter, interval, map, of, Subscription, SubscriptionLike, take } from 'rxjs';

@Component({
  selector: 'app-op',
  templateUrl: './op.component.html',
  styleUrls: ['./op.component.css']
})
export class OpComponent implements OnInit {
     my: Subscription | any;

    bookform:FormGroup |any;
  constructor() { 
  }

  ngOnInit(): void {
                  //  observable operaters: interval,pipe,take,ondestroy
    const inte=interval(1000).pipe(take(20));
   
    this.my=inte.subscribe(subt =>{

      console.log(subt);
    });

                 // observable operaters:of,map,filter

    // const off= of(1,2,3,4,5,6,7,8,9,10).pipe(map(m=>m+1),filter(f=>f==3));
     
    // off.subscribe(cri =>{
    //   console.log(cri);
    // });

   }

   ngOnDestroy(): void {
        
    this.my.unsubscribe();
   
   }

}
