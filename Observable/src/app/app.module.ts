import {  NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OpComponent } from './op/op.component';
import { ObsComponent } from './obs/obs.component';
import { FormControl } from '@angular/forms';
import { Routes } from '@angular/router';

  const routes:Routes=[
 {path:'',component:OpComponent},
{path:'obs',component:ObsComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    OpComponent,
    ObsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormControl,
    RouterModule.forRoot(routes)
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
